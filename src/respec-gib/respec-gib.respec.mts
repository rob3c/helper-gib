import { respecfully, iReckon, ifWe, firstOfAll, firstOfEach } from './respec-gib.mjs';
const maam = `[${import.meta.url}]`, sir = maam;
// console.log(`sir: ${sir}`);
import { TRUTHY_VALUES, FALSY_VALUES } from './respec-gib-constants.mjs';

await respecfully(sir, 'firstOfAll only', async () => {
    let firstOfAllTriggered = false;

    firstOfAll(sir, async () => {
        firstOfAllTriggered = true;
    });

    await ifWe(sir, `do an ifWe block, firstOfAll should've triggered`, async () => {
        iReckon(sir, firstOfAllTriggered).isGonnaBeTrue();
    });
});

await respecfully(sir, 'firstOfEach only', async () => {
    let firstOfEachObj: any = { x: 0 }
    let lastFirstOfEachValue = 0;

    firstOfEach(sir, async () => {
        lastFirstOfEachValue = firstOfEachObj.x;
        firstOfEachObj.x = firstOfEachObj.x + 1;
    });

    await ifWe(sir, `1 do an ifWe block, firstOfEach should've triggered 1 time(s)`, async () => {
        iReckon(sir, firstOfEachObj.x).willEqual(1);
    });
    await ifWe(sir, `2 do an ifWe block, firstOfEach should've triggered 2 time(s)`, async () => {
        iReckon(sir, firstOfEachObj.x).willEqual(2);
    });
});

await respecfully(sir, 'firstOfAll and firstOfEach', async () => {
    let firstOfAllTriggered = false;
    let firstOfEachObj: any = { x: 0 }
    let lastFirstOfEachValue = 0;

    firstOfAll(sir, async () => {
        firstOfAllTriggered = true;
    });
    firstOfEach(sir, async () => {
        lastFirstOfEachValue = firstOfEachObj.x;
        firstOfEachObj.x = firstOfEachObj.x + 1;
    });

    await ifWe(sir, `do an ifWe block, firstOfAll should've triggered`, async () => {
        iReckon(sir, firstOfAllTriggered).isGonnaBeTrue();
        iReckon(sir, firstOfEachObj.x).willEqual(1);
    });

    await ifWe(sir, `1 do an ifWe block, firstOfEach should've triggered 1 time(s)`, async () => {
        iReckon(sir, firstOfAllTriggered).isGonnaBeTrue();
        iReckon(sir, firstOfEachObj.x).willEqual(2);
    });
    await ifWe(sir, `2 do an ifWe block, firstOfEach should've triggered 2 time(s)`, async () => {
        iReckon(sir, firstOfAllTriggered).isGonnaBeTrue();
        iReckon(sir, firstOfEachObj.x).willEqual(3);
    });
});

await respecfully(sir, 'asTo (also tests justMetaTesting)', async () => {
    await ifWe(sir, 'should add the extra label context with a failed reckoning', () => {
        const reckoning = iReckon(sir, true).asTo('extra msg 42').isGonnaBeFalse({ justMetaTesting: true });
        iReckon(sir, reckoning.failMsg).includes('extra msg 42');
    })
});

await respecfully(sir, 'when warning wha...', async () => {
    await ifWe(sir, 'testing empty ifWe warning IGNORE THIS WARNING YO', async () => { });
});

await respecfully(sir, 'nested outer', async () => {
    await ifWe(sir, 'ifWe before nested inner', () => {
        iReckon(sir, true).isGonnaBeTrue();
    });
    await respecfully(sir, 'nested inner respecfully', async () => {
        await ifWe(sir, 'ifWe inside nested inner', () => {
            iReckon(sir, true).isGonnaBeTrue();
        });
    });
});

await respecfully(sir, `Reckoning methods`, async () => {

    await respecfully(sir, 'willEqual', async () => {
        await ifWe(sir, 'have two numbers', () => {
            iReckon(sir, 1).willEqual(1);
            iReckon(sir, 1).not.willEqual(2);
        });
        await ifWe(sir, 'have two strings', () => {
            iReckon(sir, 'string 1').willEqual('string 1');
            iReckon(sir, 'string 2').not.willEqual('string 1');
        });
        await ifWe(sir, 'have two booleans', () => {
            iReckon(sir, true).willEqual(true);
            iReckon(sir, true).not.willEqual(false);
        });
        await ifWe(sir, 'compare the same object to itself', () => {
            const a = { x: 1, b: 2 };
            iReckon(sir, a).willEqual(a);
        });
        await ifWe(sir, 'compare the same object to another simple object with same values', () => {
            const a = { x: 1, b: 2 };
            const b = { x: 1, b: 2 };
            iReckon(sir, a).willEqual(b);
        });
    });

    await respecfully(sir, 'toBe', async () => {
        await ifWe(sir, 'have two numbers', () => {
            iReckon(sir, 1).isGonnaBe(1);
            iReckon(sir, 1).not.isGonnaBe(2);
        });
        await ifWe(sir, 'have two strings', () => {
            iReckon(sir, 'string 1').isGonnaBe('string 1');
            iReckon(sir, 'string 2').not.isGonnaBe('string 1');
        });
        await ifWe(sir, 'have two booleans', () => {
            iReckon(sir, true).isGonnaBe(true);
            iReckon(sir, true).not.isGonnaBe(false);
        });
        await ifWe(sir, 'compare the same object to itself', () => {
            const a = { x: 1, b: 2 };
            iReckon(sir, a).isGonnaBe(a);
        });
        await ifWe(sir, 'compare the same object to another simple object with same values', () => {
            const a = { x: 1, b: 2 };
            const b = { x: 1, b: 2 };
            iReckon(sir, a).isGonnaBe(b);
        });
    });

    await respecfully(sir, 'isGonnaBeTruthy', async () => {

        await ifWe(sir, 'do some truthy values', () => {
            for (let i = 0; i < TRUTHY_VALUES.length; i++) {
                const value = TRUTHY_VALUES[i];
                iReckon(sir, value).isGonnaBeTruthy();
            }
            for (let i = 0; i < TRUTHY_VALUES.length; i++) {
                const value = TRUTHY_VALUES[i];
                const reckoning = iReckon(sir, value).not.isGonnaBeTruthy({ justMetaTesting: true });
                iReckon(sir, reckoning.failMsg).isGonnaBeTruthy();
            }
        });
        await ifWe(sir, 'do some falsy values', () => {
            for (let i = 0; i < FALSY_VALUES.length; i++) {
                const value = FALSY_VALUES[i];
                const reckoning = iReckon(sir, value).isGonnaBeTruthy({ justMetaTesting: true });
                iReckon(sir, reckoning.failMsg).isGonnaBeTruthy();
            }
            for (let i = 0; i < FALSY_VALUES.length; i++) {
                const value = FALSY_VALUES[i];
                iReckon(sir, value).not.isGonnaBeTruthy();
            }
        });
    });

    await respecfully(sir, 'isGonnaBeFalsy', async () => {

        await ifWe(sir, 'do some falsy values', () => {
            for (let i = 0; i < FALSY_VALUES.length; i++) {
                const value = FALSY_VALUES[i];
                iReckon(sir, value).isGonnaBeFalsy();
            }
            for (let i = 0; i < FALSY_VALUES.length; i++) {
                const value = FALSY_VALUES[i];
                const reckoning = iReckon(sir, value).not.isGonnaBeFalsy({ justMetaTesting: true });
                iReckon(sir, reckoning.failMsg).isGonnaBeTruthy(); // the failMsg** is truthy!
            }
        });
        await ifWe(sir, 'do some truthy values', () => {
            for (let i = 0; i < TRUTHY_VALUES.length; i++) {
                const value = TRUTHY_VALUES[i];
                const reckoning = iReckon(sir, value).isGonnaBeFalsy({ justMetaTesting: true });
                iReckon(sir, reckoning.failMsg).isGonnaBeTruthy(); // the failMsg** is truthy!
            }
            for (let i = 0; i < TRUTHY_VALUES.length; i++) {
                const value = TRUTHY_VALUES[i];
                iReckon(sir, value).not.isGonnaBeFalsy();
            }
        });
    });
    await respecfully(sir, 'includes', async () => {
        await ifWe(sir, 'test for inclusion', () => {
            iReckon(sir, [1, 2, 3]).includes(1);
        });
        await ifWe(sir, 'test for not inclusion', () => {
            iReckon(sir, [1, 2, 3]).not.includes('a');
            iReckon(sir, [1, 2, 3]).not.includes(4, { addedMsg: 'added msg yo' });
        });
    });
});
