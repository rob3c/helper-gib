import { labelize } from "./respec-gib-helper.mjs";

// DO NOT DECLARE FILE-LEVEL LOGALOT, AS THIS VARIABLE IS SET LOCALLY IN THIS FILE'S FUNCTIONS.

export interface InfoBase {
    /**
     * Log context.
     */
    lc?: string;
    /**
     * If true, performs lots of trace verbose logging respeculation stuff.
     */
    logalot?: boolean;
}

export type MaybeAsyncFn<TReturn = void> =
    (() => TReturn) | (() => Promise<TReturn>);

export interface RespecFunctionInfo extends InfoBase {
    fn: MaybeAsyncFn;
}

export interface IfWeBlock extends InfoBase {

}

export interface RespecInfo extends InfoBase {
    /**
     * file path of spec. use meta.import.url
     */
    title: string;
    kind: 'respecfully' | 'ifwe';
    bodyFn: MaybeAsyncFn;
    parent?: RespecInfo;
    subBlocks: RespecInfo[];
    /**
     * function(s) that execute before any respeculations.
     */
    fnFirstOfAlls: MaybeAsyncFn[];
    fnFirstOfAllsComplete?: boolean;
    /**
     * function(s) that execute before each respeculation.
     */
    fnFirstOfEachs: MaybeAsyncFn[];
    /**
     * If the respecful block contains a reckoning via `iReckon`, then this will
     * be contain the resulting object of that reckoning.
     */
    reckonings: Reckoning[];
    /**
     * if true, block as completed execution
     */
    complete?: boolean;
    error?: any;
    /**
     * todo: stopOnFailure
     */
    // stopOnFailure: false,
}

export interface RespecOptions {
    // stopOnFailure?: boolean;
    overrideLc?: string;
    logalot?: boolean;
    /**
     * If true, then we're just checking the respec lib itself. This will not
     * include the block/reckoning in the final report.
     */
    justMetaTesting?: boolean;
}

export class RespecGib {
    respecs: { [title: string]: RespecInfo[] } = {};
}

/**
 * todo: fill this out with whitelisted function names.
 */
export type ReckoningFunctionName = 'willEqual' | string;

export interface ReckoningOptions {
    overrideLc?: string;
    addedMsg?: string;
    /**
     * If true, then we're just checking the respec lib itself. This will not
     * include the block/reckoning in the final report.
     */
    justMetaTesting?: boolean;
}

export interface ReckoningInfo {
    /**
     * fn name to execute
     */
    fnName?: ReckoningFunctionName | undefined;
    /**
     * need to rename. This is what the reckoning is testing against this.value
     * for some tests.
     */
    x?: any | undefined;
    /**
     * Optional addedMsg
     */
    addedMsg: string | undefined;
}

export class Reckoning {
    private extraLabel: string | undefined = undefined;

    failMsg: string | undefined = undefined;
    justMetaTesting: boolean | undefined = undefined;

    #not: boolean = false;
    get not(): Reckoning {
        this.#not = !this.#not;
        return this;
    }

    completed: Promise<boolean> | undefined = undefined;

    constructor(
        private value: any,
        private logalot: boolean = false,
    ) {
    }

    asTo(extraLabel: string): Reckoning {
        this.extraLabel = extraLabel;
        return this;
    }

    /**
     * checks for strict equals ===.
     *
     * @param x value to test against this.value
     * @param opts
     * @returns true if passes, false if fails
     */
    willEqual(x: any, opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.willEqual.name}]`;
        const { logalot } = this;
        const { addedMsg, justMetaTesting } = opts ?? {};
        try {
            if (logalot) { console.log(`${lc} starting... (I: a07da12997b56d62347be2a71d984a23)`); }
            this.justMetaTesting = justMetaTesting;
            if (typeof x !== typeof this.value && !this.#not) {
                throw new Error(`Uh oh. this.value (${labelize(this.value)}) type (${typeof this.value}) is a different than ${labelize(x)} type (${typeof x}) ${addedMsg ? '(' + addedMsg + ')' : ''}(E: cdb0a3a75b611facfa6d80ff14839323)`);
            }
            if (typeof x === 'object') {
                if (!this.#not) {
                    if (this.value !== x) {
                        const thisValueString = JSON.stringify(this.value);
                        const xString = JSON.stringify(x);
                        if (thisValueString !== xString) {
                            throw new Error(`object comparison using JSON.stringify says these two objects are different. (E: 8375bab01f49dcac280c9dde0e120423)`);
                        }
                        // throw new Error(`clever object comparison is not implemented yet (E: 067c62f1767e25ab39f6aeffccb36e23)`);
                    }
                } else {
                    if (this.value === x) {
                        throw new Error(`Uh oh. Objects were supposed to be different but they are the same instance of the same object. (E: 3e62e334af53d37686f4009ca8274523)`);
                    }
                    const thisValueString = JSON.stringify(this.value);
                    const xString = JSON.stringify(x);
                    if (thisValueString === xString) {
                        throw new Error(`object comparison using JSON.stringify says these two objects are the same. (E: f661001788a440d08a54bea2e57df580)`);
                    }
                }
            } else {
                if (!this.#not) {
                    if (this.value !== x) {
                        throw new Error(`Uh oh. this.value (${labelize(this.value)}) does not strict equal ${labelize(x)} ${addedMsg ? '(' + addedMsg + ')' : ''}(E: a568969e5729ddb5176e7c76fbdd7b23)`);
                    }
                } else {
                    if (this.value === x) {
                        throw new Error(`Uh oh. this.value (${labelize(this.value)}) does strict equal ${labelize(x)} ${addedMsg ? '(' + addedMsg + ')' : ''}(E: cc68058d7fa84fbebee8d00cb705df69)`);
                    }
                }
            }
        } catch (error) {
            // const msg = `${lc} ${error.message}`;
            const msg = this.getErrorMsgLabeledAndWhatnot(lc, addedMsg, error);
            this.failMsg = msg;
        } finally {
            this.completed = Promise.resolve(true);
            if (logalot) { console.log(`${lc} complete.`); }
        }
        return this;
    }

    /**
     * checks for strict equals ===.
     * the same as willEqual atow
     *
     * @param x value to test against this.value
     * @param opts
     * @returns true if passes, false if fails
     */
    isGonnaBe(x: any, opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.isGonnaBe.name}]`;
        return this.willEqual(x, { ...opts, overrideLc: lc });
    }

    /**
     * checks for truthy of this.value
     *
     * @param opts
     * @returns true if passes, false if fails
     */
    isGonnaBeTruthy(opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.isGonnaBeTruthy.name}]`;
        const { logalot } = this;
        const { addedMsg, justMetaTesting } = opts ?? {};
        try {
            if (logalot) { console.log(`${lc} starting... (I: 8f3e2afedf3b4944bd2d824625ebfb00)`); }
            this.justMetaTesting = justMetaTesting;
            if (!this.#not) {
                if (!this.value) {
                    throw new Error(`Uh oh. Ain't truthy. this.value (${labelize(this.value)}) ${addedMsg ? '(' + addedMsg + ')' : ''}(E: 699e9465b6d64c6fb53a533024751acd)`);
                }
            } else {
                if (this.value) {
                    throw new Error(`Uh oh. It is truthy. this.value (${labelize(this.value)}) ${addedMsg ? '(' + addedMsg + ')' : ''}(E: 061557a7679e4e78ad38d3a9b83c8f8d)`);
                }
            }
        } catch (error) {
            // const msg = `${lc} ${error.message}`;
            const msg = this.getErrorMsgLabeledAndWhatnot(lc, addedMsg, error);
            this.failMsg = msg;
        } finally {
            this.completed = Promise.resolve(true);
            if (logalot) { console.log(`${lc} complete.`); }
        }
        return this;
    }
    isGonnaBeFalsy(opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.isGonnaBeFalsy.name}]`;
        // const { logalot } = this;
        // const { addedMsg, justMetaTesting } = opts ?? {};
        return this.not.isGonnaBeTruthy({ ...opts, overrideLc: lc });
    }

    /**
     * checks for this.value === true (unless not'd)
     *
     * @param opts
     * @returns true if passes, false if fails
     */
    isGonnaBeTrue(opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.isGonnaBeTrue.name}]`;
        const { logalot } = this;
        const { addedMsg, justMetaTesting } = opts ?? {};
        try {
            this.justMetaTesting = justMetaTesting;
            if (logalot) { console.log(`${lc} starting... (I: aade114a8ae7495a9d286a3ebc9be5ba)`); }
            if (!this.#not) {
                if (this.value !== true) {
                    throw new Error(`Uh oh. this.value ain't true: (${labelize(this.value)}) ${addedMsg ? '(' + addedMsg + ')' : ''}(E: aeca1221ba994f9eafef8f482e73f941)`);
                }
            } else {
                if (this.value === true) {
                    throw new Error(`Uh oh. It is true. this.value (${labelize(this.value)}) ${addedMsg ? '(' + addedMsg + ')' : ''}(E: cd61a7efa6034c7d94daf9f8f43cdaff)`);
                }
            }
        } catch (error) {
            // const msg = `${lc} ${error.message}`;
            const msg = this.getErrorMsgLabeledAndWhatnot(lc, addedMsg, error);
            this.failMsg = msg;
        } finally {
            this.completed = Promise.resolve(true);
            if (logalot) { console.log(`${lc} complete.`); }
        }
        return this;
    }

    /**
     * checks for this.value === false (unless not'd)
     *
     * @param opts
     * @returns true if passes, false if fails
     */
    isGonnaBeFalse(opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.isGonnaBeFalse}]`;
        return this.willEqual(false, { ...opts, overrideLc: lc });
    }

    /**
     * checks for this.value === undefined (unless not'd)
     *
     * @param opts
     * @returns true if passes, false if fails
     */
    isGonnaBeUndefined(opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.isGonnaBeUndefined.name}]`;
        const { logalot } = this;
        return this.willEqual(undefined, { ...opts, overrideLc: lc }); // checking this out
    }

    /**
     * checks for this.value.includes(x) (unless not'd)
     *
     * @param x value to check for inclusion in this.value
     * @param opts
     * @returns true if passes, false if fails
     */
    includes(x: any, opts?: ReckoningOptions): Reckoning {
        const lc = `[${this.includes.name}]`;
        const { logalot } = this;
        const { addedMsg, justMetaTesting } = opts ?? {};
        try {
            if (logalot) { console.log(`${lc} starting... (I: d270ce3e08284de3906447f508f8e912)`); }
            this.justMetaTesting = justMetaTesting;
            if (Array.isArray(this.value)) {
                let arr = this.value as any[];
                if (!this.#not) {
                    if (!arr.includes(x)) {
                        throw new Error(`Uh oh. this.value (${labelize(this.value)}) does NOT include ${labelize(x)} (E: c4a17172d5fbe6b637256b1c56c5c823)`);
                    }
                } else {
                    if (arr.includes(x)) {
                        throw new Error(`Uh oh. this.value (${labelize(this.value)}) DOES include ${labelize(x)} (E: c4a17172d5fbe6b637256b1c56c5c823)`);
                    }
                }
            } else if (typeof this.value === 'string') {
                if (typeof x !== 'string') { throw new Error(`Uh oh. this.value is a string but testing against a non-string inclusion? Terribly sorry to disturb you on this one... (E: 962b88c207b5e701aaa2cb757bd97b23)`); }
                let valueStr = this.value as string;
                let xStr = x as string;
                if (!this.#not) {
                    if (!valueStr.includes(xStr)) {
                        throw new Error(`Uh oh. this.value (${labelize(this.value)}) does NOT include ${labelize(x)} (E: 28b572ece3364c669377f5431a5dcba8)`);
                    }
                } else {
                    if (valueStr.includes(xStr)) {
                        throw new Error(`Uh oh. this.value (${labelize(this.value)}) DOES include ${labelize(x)} (E: 7c041dd5e43c400fb8edc0460c9a1e4f)`);
                    }
                }
            } else {
                throw new Error(`Uh oh. this.value isn't an array nor a string, if you don't mind me saying. (E: a73a04323478016fc632d8697d695223)`);
            }
        } catch (error) {
            // const msg = `${lc} ${error.message}`;
            debugger;
            const msg = this.getErrorMsgLabeledAndWhatnot(lc, addedMsg, error);
            this.failMsg = msg;
        } finally {
            this.completed = Promise.resolve(true);
            if (logalot) { console.log(`${lc} complete.`); }
        }
        return this;
    }

    // #region helper functions

    /**
     * long silly names means I need to refactor them. it's an indicator, conscious decision. eesh.
     * @param lc log context
     * @param error catch block error
     * @returns error msg with extraLabel if assigned
     */
    getErrorMsgLabeledAndWhatnot(lc: string, addedMsg: string | undefined, error: any): string {
        let msg: string = lc;
        if (this.extraLabel) { msg += `[${this.extraLabel}]`; }
        if (addedMsg) { msg += `[${addedMsg}]`; }
        msg += ` ${error.message}`;
        return msg;
    }

    // #endregion helper functions

}

/**
 * This global object via `globalThis.respecGib` contains the
 * state of respec(s) to be/being/been executed.
 *
 * @returns the current respecGib object
 */
export function globalRespecGib(): RespecGib {
    if (!(globalThis as any).respecGib) {
        (globalThis as any).respecGib = new RespecGib();
    }
    return (globalThis as any).respecGib;
}

/**
 * gets the most recent non-nested (non-subblock) respecInfo from the global
 * state for the given `title`.
 *
 * @param title unique to file, use import.meta.url
 * @returns respecInfo for the given `title`
 */
export function getRespec(title: string): RespecInfo {
    const { respecs } = globalRespecGib();
    const existingInfos = respecs[title] ?? [];
    if (existingInfos.length > 0) {
        return existingInfos.at(-1)!;
    } else {
        // no existing infos
        throw new Error(`no respec yet (E: abb1ca2f20fa521c92713ce81029ca23)`);
    }
}

/**
 * gets the most recent non-complete sub-block or if none, returns the
 * incoming respecInfo block.
 *
 * @param block root respecInfo block
 */
export function getActiveRespecfullyBlock(block: RespecInfo): RespecInfo {
    const activeSubblocks = block.subBlocks.filter(x => x.kind === 'respecfully' && !x.complete);
    if (activeSubblocks.length === 0) {
        return block;
    } else if (activeSubblocks.length === 1) {
        return getActiveRespecfullyBlock(activeSubblocks[0]);
    } else {
        throw new Error(`should only be one non-complete subblock (E: 677b425e787ebb111107882ef7136223)`);
    }
}

export async function openRespecfullyBlock(block: RespecInfo): Promise<void> {
    const { respecs } = globalRespecGib();
    let respecBlocks = respecs[block.title];
    if (!respecBlocks) {
        respecBlocks = [];
        respecs[block.title] = respecBlocks;
    }
    const mostRecentBlock = respecBlocks.at(-1);
    if (!mostRecentBlock || mostRecentBlock.complete) {
        respecBlocks.push(block);
    } else {
        let parent = getActiveRespecfullyBlock(mostRecentBlock);
        parent.subBlocks.push(block);
        block.parent = parent;
        block.logalot = parent.logalot;
        block.lc = `${parent.lc}${block.lc}`;
        block.fnFirstOfEachs = parent.fnFirstOfEachs.concat();
    }
}

export async function closeRespecBlock(block: RespecInfo, logalot?: boolean): Promise<void> {
    const lc = `[${closeRespecBlock.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 31a2d6ae8edb711b68b1f439c2fa1923)`); }
        if (!logalot) {
            logalot = block.logalot;
            if (logalot) { console.log(`${lc} starting... (I: e4524212e0dc436c8faf0a998aafb630)`); }
        }
        const ifWeBlocks_NonErrored = block.subBlocks.filter(x => x.kind === 'ifwe').filter(x => !x.error);
        const reckonings = ifWeBlocks_NonErrored.flatMap(x => x.reckonings).filter(r => !r.justMetaTesting);
        await Promise.all(reckonings.map(r => r.completed));
        const prefix = `[${block.title.split('/').at(-1)!}${block.lc}`;
        let passes: string[] = [];
        let fails: Reckoning[] = [];
        for (let reckoning of reckonings) {
            // good enough for now. need to refactor to abstract away logging
            // the success/failure to work in both browser and in node
            const emojifail = (s: string) => {
                return `💔 ${prefix}${s}`;
            }
            if (reckoning.failMsg) {
                console.error('\x1b[31m%s\x1b[0m', emojifail(reckoning.failMsg));  // red
                fails.push(reckoning);
            } else {
                passes.push('💚');
            }
        }

        const totalCount = passes.length + fails.length;
        if (passes.length > 0) {
            // const passMsg = `${prefix} ${passes.length} of ${totalCount} ${passes.join('')} reckonings passed`;
            const passMsg = `💚 ${prefix} ${passes.length} of ${totalCount} respecful reckonings`;
            console.log('\x1b[32m%s\x1b[0m', passMsg);  // green
        }
        if (fails.length > 0) {
            const failedMsg = `${prefix} ${fails.length} of ${totalCount} DISrespecful reckonings`;
            console.log('\x1b[31m%s\x1b[0m', failedMsg);  // red
        }

        for (let i = 0; i < ifWeBlocks_NonErrored.length; i++) {
            const ifWeBlock_NonErrored = ifWeBlocks_NonErrored[i];
            const msg = `  ✅ ${prefix}${ifWeBlock_NonErrored.lc ?? ''} respecful.`;
            console.log('\x1b[32m%s\x1b[0m', msg);  // green
        }

        const ifWeBlocks_Errored = block.subBlocks.filter(x => x.kind === 'ifwe').filter(x => !!x.error);
        for (let i = 0; i < ifWeBlocks_Errored.length; i++) {
            const ifWeBlock_Errored = ifWeBlocks_Errored[i];
            const { error } = ifWeBlock_Errored;
            const failedMsg = `  🟥 ${prefix}${ifWeBlock_Errored.lc ?? ''} DISrespecful ifWe Block!! The ifWe block errored out and all reckonings are null and void...well not literally. If you please, here is the error, thank you kindly:\n\n${error.message}\nstack:\n\n${error.stack ?? 'no error stack?'}`;
            console.log('\x1b[31m%s\x1b[0m', failedMsg);  // red
        }
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        block.error = error;
        throw error; // throw?
    } finally {
        block.complete = true;
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

export async function respecfully(title: string, label: string, fn: MaybeAsyncFn, logalot?: boolean): Promise<void> {
    const lc = `[${respecfully.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 6093b8f34b1f35da6416449cd16a3223)`); }
        label ||= 'well sir/maam/other, we seemed to have perhaps forgot the label for this respecful block.';

        if (!fn) { throw new Error(`fn required (E: 23624f705087ed6c27a3ff2a616d7b23)`); }

        // build a new respec block for this instantiation.
        // nested respec will point to parent automagically
        const respecInfo: RespecInfo = {
            title,
            lc: `[${label}]`,
            logalot,
            kind: 'respecfully',
            bodyFn: fn,
            fnFirstOfAlls: [],
            fnFirstOfEachs: [],
            subBlocks: [],
            reckonings: [],
        };

        // sets the corresponding global respec info corresponding to title
        await openRespecfullyBlock(respecInfo);

        // execute the respecful body lambda
        await fn();

        await closeRespecBlock(respecInfo);
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * Execute this function ONCE before the FIRST ifWe block is executed.
 *
 * @param title unique to file, use import.meta.url
 * @param fn to execute
 * @param logalot true for verbose trace logging
 */
export function firstOfAll(title: string, fn: MaybeAsyncFn, logalot?: boolean): void {
    const lc = `[${firstOfAll.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: f06eb90d3afb4e85a121173805fbd43f)`); }

        if (!fn) { throw new Error(`fn required (E: d5ac5d6c7a144532bdc22f608ec8ef15)`); }

        const respec = getRespec(title);
        if (respec.fnFirstOfAllsComplete) { throw new Error(`cannot add a fnFirstOfAll after an ifWe block  (E: bf9cceedcae3248022ca84ecafd76623)`); }
        respec.fnFirstOfAlls.push(fn);
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * Execute this function before EACH & EVERY ifWe block is executed.
 *
 * @param title unique to file, use import.meta.url
 * @param fn to execute
 * @param logalot true for verbose trace logging
 */
export function firstOfEach(title: string, fn: MaybeAsyncFn, logalot?: boolean): void {
    const lc = `[${firstOfEach.name}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: d98af0af339c4b6cb823491f3142736b)`); }

        if (!fn) { throw new Error(`fn required (E: 7dc1bf89ed684a3080bdb219e68de4da)`); }

        let respec = getRespec(title);
        respec = getActiveRespecfullyBlock(respec);
        respec.fnFirstOfEachs.push(fn);
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * Respeculation block that should contain reckonings (iReckon functions).
 *
 * ## notes
 *
 * you can't...
 * * add firstOfEach/All after the first of these blocks
 * * add respecfully blocks inside this block
 * * nest these blocks
 *
 * @param title unique to file, use import.meta.url
 * @param label label of respec
 * @param fn to execute
 * @param logalot true for verbose trace logging
 */
export async function ifWe(title: string, label: string, fn: MaybeAsyncFn, opts?: RespecOptions): Promise<void> {
    let lc = `[${ifWe.name}][${label ?? 'no label?'}]`;
    let { logalot, overrideLc, } = opts ?? {};
    if (!!overrideLc || overrideLc === '') { lc = overrideLc; }
    try {
        if (logalot) { console.log(`${lc}[${label}] starting... (I: a321aeeab5144c518db3e03da25506bd)`); }

        if (!fn) { throw new Error(`fn required (E: 2acfc615924145618f96be53b208186b)`); }

        // get the currently executing respecfully block for this title/file
        let respec = getRespec(title);
        respec = getActiveRespecfullyBlock(respec);
        if (!logalot) {
            if (respec.logalot) { logalot = true; }
            if (logalot) { console.log(`${lc}[${label}] starting... (I: 38dac8ce84604b8991bfe88f4947f5a2)`); }
        }
        const ifWeBlock: RespecInfo = {
            title,
            logalot,
            kind: 'ifwe',
            reckonings: [],
            fnFirstOfAlls: [],
            fnFirstOfEachs: [],
            subBlocks: [],
            lc: `[${label}]`,
            bodyFn: fn,
        };
        respec.subBlocks.push(ifWeBlock);
        if (!respec.fnFirstOfAllsComplete) {
            for (let i = 0; i < respec.fnFirstOfAlls.length; i++) {
                await respec.fnFirstOfAlls[i]();
            }
            respec.fnFirstOfAllsComplete = true;
        }
        for (let i = 0; i < respec.fnFirstOfEachs.length; i++) {
            const fnFirstOfEach = respec.fnFirstOfEachs[i];
            if (logalot) { console.log(`${lc} firstOfEach executing for '${label}' (I: 849fa60aea0561aa5d3bd52755ac7823)`); }
            try {
                await fnFirstOfEach();
            } catch (error) {
                ifWeBlock.error = error;
                ifWeBlock.complete = true;
                return; /* <<<< returns early */
            }
        }


        // see how many reckonings we start with, to compare with after fn exec
        const initialReckoningCount = ifWeBlock.reckonings.length;

        // execute the respec body
        try {
            await fn();
        } catch (error) {
            ifWeBlock.error = error;
            ifWeBlock.complete = true;
            return; /* <<<< returns early */
        }

        // compare reckoning count, because it's unexpected if the user didn't
        // have a reckoning in this block
        const postBlockCount = ifWeBlock.reckonings.length;
        if (initialReckoningCount === postBlockCount) {
            const prefix = `[${ifWeBlock.title.split('/').at(-1)!}${ifWeBlock.lc}`;
            console.warn('\x1b[33m%s\x1b[0m', `${prefix}${lc} There were no iReckon reckonings in this ifWe block. Did you forget to respec yourself? (W: c188125dd7cc4100821f4c9c6cb1462a)`);
        }
        ifWeBlock.complete = true;
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        // throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}

/**
 * starts a reckoning on what you figure.
 *
 * ## notes
 *
 * * this is similar to an expectation or assertion in other worlds, but way better.
 *
 * @param title unique to file, use import.meta.url
 * @param x value to test against this.value
 * @returns the reckoning instance
 */
export function iReckon(title: string, x: any): Reckoning {
    // const reckoning = new Reckoning(clone(x));
    const reckoning = new Reckoning(x);
    const respecInfo = getRespec(title);
    const activeBlock = getActiveRespecfullyBlock(respecInfo);
    const currentIfWe = activeBlock.subBlocks.filter(x => x.kind === 'ifwe').at(-1);
    if (!currentIfWe) { throw new Error(`iReckon can only occur inside of an ifWe block (E: 6f3cc46b8d9c22263b4c37121e26b623)`); }
    currentIfWe.reckonings.push(reckoning);
    return reckoning;
}
