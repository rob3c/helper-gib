import { readdir } from 'node:fs/promises';
import { statSync } from 'node:fs';
import * as pathUtils from 'path';

import { HELPER_LOG_A_LOT } from './constants.mjs';
import { pretty } from './helpers/utils-helper.mjs';

/**
 * This is how I enable/disable verbose logging. Do with it what you will.
 */
const logalot = HELPER_LOG_A_LOT || false;

// #region 1. get respec paths

/**
 * set this to the root of the respecs to look at
 */
const RESPEC_ROOT_DIR_RELATIVE_TO_BASE = './dist';
/**
 * change this to suit your naming convention
 */
const RESPEC_FILE_REG_EXP = /^.+respec\.mjs$/;

const basePath = process.cwd();
const srcPath = pathUtils.join(basePath, RESPEC_ROOT_DIR_RELATIVE_TO_BASE);

if (logalot) { console.log(`cwd: ${process.cwd()}`); }
if (logalot) { console.log(`basePath: ${basePath}`); }
if (logalot) { console.log(`srcPath: ${srcPath}`); }

/**
 * builds a list of respec file paths, recursively traversing subdirectories
 * starting from `dirPath`.
 *
 * @param dirPath a full path corresponding to a directory
 * @param found respec paths already found (used in recursive calls)
 * @returns list of all respec paths according to the respec regexp constant {@link RESPEC_FILE_REG_EXP}
 */
async function getRespecFileFullPaths(dirPath: string, found: string[]): Promise<string[]> {
    const lc = `[${getRespecFileFullPaths.name}][${dirPath}]`;
    try {
        if (logalot) { console.log(`${lc} starting... (I: 16026290523925f79ba1933847e2a623)`); }
        found ??= [];
        const children = await readdir(dirPath);
        if (logalot) { for (let i = 0; i < children.length; i++) { console.log(children[i]); } }
        const files: string[] = [];
        const dirs: string[] = [];
        children.forEach(name => {
            const fullPath = pathUtils.join(dirPath, name);
            const stat = statSync(fullPath);
            if (stat.isDirectory()) {
                // symbolic link could create a loop
                if (!stat.isSymbolicLink()) { dirs.push(fullPath); }
            } else if (!!name.match(RESPEC_FILE_REG_EXP)) {
                files.push(fullPath);
            }
        });

        found = found.concat(files);
        for (let i = 0; i < dirs.length; i++) {
            const subfound = await getRespecFileFullPaths(dirs[i], found);
            found = found.concat(subfound);
        }
        return Array.from(new Set(found));
    } catch (error) {
        console.error(`${lc} ${error.message}`);
        throw error;
    } finally {
        if (logalot) { console.log(`${lc} complete.`); }
    }
}
// #endregion 1. get respec paths

// #region 2. execute paths' respective respecs

// for now, we'll do sequentially, but in the future we could conceivable farm
// these out to other node processes, or at least Promise.all

const respecPaths = await getRespecFileFullPaths(srcPath, []);
if (logalot) { console.log(respecPaths); }
for (let i = 0; i < respecPaths.length; i++) {
    const respecPath = respecPaths[i];

    // 1. import the file, which should compile the information for the
    // tests for that file, using the polite title as the uuid for that
    // file.
    //
    // 2. importing the file's module executes those respecs
    if (logalot) { console.log(respecPath); }
    const esm = await import(respecPath);
    if (logalot) { console.log(pretty(Object.keys(esm))); }
}

// #endregion 2. execute paths' respective respecs
