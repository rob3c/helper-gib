## 0.0.1/2
* meta: initial commit
  * getting basic folder structure going
  * typescript + es modules
  * testing with both node and browser via jasmine.
  * already has some of the core helper functionality from ts-gib and ionic-gib.
* meta: reorg and ts-gib functions
  * slight cleanup of organization helper files/folders.
    * changed "common" to "helpers" folder
    * added index.mts
    * exporting in base index.mts via helpers/index.mts instead of individual files.
  * incorporated helper functions from ts-gib.
  * all tests passing. This should be ready to consume in ts-gib (or close to it).
